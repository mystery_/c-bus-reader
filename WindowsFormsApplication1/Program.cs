﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Runtime.InteropServices;


namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);
        private const int ATTACH_PARENT_PROCESS = -1;

        public static string port2connect;
        private static Form1 form;
        private static SerialPort test;

        [STAThread]
        private static void Main()
        {
     //       AttachConsole(ATTACH_PARENT_PROCESS);
            
            Form1 form = new Form1();

           
           
            string[] ports = GetPorts(); 
            foreach (string port in ports)
            {
                form.addport(port);
            }
            
            test = new System.IO.Ports.SerialPort();

            
            Application.EnableVisualStyles();
 //           Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(form);

        }



        private static string[]  GetPorts()
        {
            // Get a list of serial port names.
            string[] ports = SerialPort.GetPortNames();

            Console.WriteLine("The following serial ports were found:");

            // Display each port name to the console.
            foreach (string port in ports)
            {
                Console.WriteLine(port);
            }
            return ports;
        }


        public static void HandleConnection()
        {
            test.PortName = port2connect;
            test.BaudRate = 250000;
            test.Parity = Parity.None;
            test.DataBits = 8;
            test.StopBits = StopBits.One;
            test.Handshake = Handshake.None;
            test.Open();
        }
    } 
}
